#17     http://www.linuxquestions.org/questions/linux-wireless-networking-41/after-install-some-new-protocol-in-ns2-35-it-appears-some-mistake-when-i-run-the-tcl-file-4175538323/page2.html

 
#      20 Jan 2015, @billyfuad / @Muhammed Fuad  ←  mail

set stopTime 50
set BO 3
set SO 3
set speed 1
#speed is in m/s
 #======================================================================
# Default Script valions
# ======================================================================
set opt(chan)		Channel/WirelessChannel
set opt(prop)		Propagation/TwoRayGround
set opt(netif)		Phy/WirelessPhy/802_15_4
set opt(mac)		Mac/802_15_4
set opt(ifq)		Queue/DropTail/PriQueue	
set opt(ll)		LL
set opt(ant)            Antenna/OmniAntenna
set opt(x)		300		;# koordinat X untuk topologi
set opt(y)		300		;# koordinat Y untuk topologi

set opt(ifqlen)		50		;# paket maksimal ifq
set opt(nn)		15		;# jumlah nodes
set opt(seed)		0.0
set opt(stop)		50		;# waktu simulasi
set opt(tr)		gpsr50.tr	;# trace file
set opt(nam)            gpsr50.nam
set opt(rp)             gpsr		;# routing protocol script (Greedy Perimeter Stateless Routing)
set opt(lm)             "off"		;# log movement
set opt(traffic)	cbr             ;# cbr
set opt(energy)         EnergyModel
set opt(AnH)            1.5             ;# Antenna Height
set PI                  3.1415926
set opt(Pt)             25.0 		;# Transmission Power/Range in meters
set opt(initialenergy)  1            ;# Initial energy in Joules
# ======================================================================

LL set mindelay_		50us
LL set delay_			25us
LL set bandwidth_		0	;# not used

Agent/Null set sport_		0
Agent/Null set dport_		0

Agent/CBR set sport_		0
Agent/CBR set dport_		0

Agent/TCPSink set sport_	0
Agent/TCPSink set dport_	0

Agent/TCP set sport_		0
Agent/TCP set dport_		0
Agent/TCP set packetSize_	60

Queue/DropTail/PriQueue set Prefer_Routing_Protocols    1

# Agent/GPSR setting
Agent/GPSR set planar_type_  1   ;#1=GG planarize, 0=RNG planarize
Agent/GPSR set hello_period_   5.0 ;#Hello message period


# unity gain, omni-directional antennas
# set up the antennas to be centered in the node and 1.5 meters above it
Antenna/OmniAntenna set X_ 0
Antenna/OmniAntenna set Y_ 0
Antenna/OmniAntenna set Z_ 1.5
Antenna/OmniAntenna set Gt_ 1.0
Antenna/OmniAntenna set Gr_ 1.0

# Initialize the SharedMedia interface with parameters to make
# it work like the 914MHz Lucent WaveLAN DSSS radio interface
Phy/WirelessPhy set CPThresh_ 10.0
Phy/WirelessPhy set CSThresh_ 8.91754e-10  ;#sensing range of 200m
Phy/WirelessPhy set RXThresh_ 8.91754e-10  ;#communication range of 200m
Phy/WirelessPhy set Rb_ 2*1e6
Phy/WirelessPhy set freq_ 914e+6 
Phy/WirelessPhy set L_ 1.0


# The transimssion radio range 
#Phy/WirelessPhy set Pt_ 6.9872e-4    ;# ?m
#Phy/WirelessPhy set Pt_ 8.5872e-4    ;# 40m
#Phy/WirelessPhy set Pt_ 1.33826e-3   ;# 50m
#Phy/WirelessPhy set Pt_ 7.214e-3     ;# 100m
Phy/WirelessPhy set Pt_ 0.2818       ;# 250m
#Phy/WirelessPhy set Pt_ 2.28289e-11 ;# 500m
# ======================================================================

#yang dihapus new

proc usage { argv0 }  {
	puts "Usage: $argv0"
	puts "\tmandatory arguments:"
	puts "\t\t\[-x MAXX\] \[-y MAXY\]"
	puts "\toptional arguments:"
	puts "\t\t\[-cp conn pattern\] \[-sc scenario\] \[-nn nodes\]"
	puts "\t\t\[-seed seed\] \[-stop sec\] \[-tr tracefile\]\n"
}


proc getopt {argc argv} {
	global opt
	lappend optlist cp nn seed sc stop tr x y

	for {set i 0} {$i < $argc} {incr i} {
		set arg [lindex $argv $i]
		if {[string range $arg 0 0] != "-"} continue

		set name [string range $arg 1 end]
		set opt($name) [lindex $argv [expr $i+1]]
	}
}

set appTime1            2.0	;# in seconds 
set appTime2            2.1	;# in seconds 
set appTime3            2.2	;# in seconds 
set appTime4            2.3	;# in seconds 
set appTime5            2.4	;# in seconds 
set stopTime            50	;# in seconds 


#======================================================================
# Parameter untuk 802.11p
#======================================================================

Mac/802_15_4 put-nam-traceall (# nam4wpan #)	;# inform nam that this is a trace file for wpan (special handling needed)
Mac/802_15_4 wpanCmd verbose on
Mac/802_15_4 wpanNam namStatus on	;# default = off (should be turned on before other 'wpanNam' commands can work)
#Mac/802_15_4 wpanNam ColFlashClr gold	;# default = gold
Mac/802_15_4 wpanNam PANCoorClr tomato
Mac/802_15_4 wpanNam CoorClr blue
Mac/802_15_4 wpanNam DevClr green
# For model 'TwoRayGround'
set dist(5m)  7.69113e-06
set dist(9m)  2.37381e-06
set dist(10m) 1.92278e-06
set dist(11m) 1.58908e-06
set dist(12m) 1.33527e-06
set dist(13m) 1.13774e-06
set dist(14m) 9.81011e-07
set dist(15m) 8.54570e-07
set dist(16m) 7.51087e-07
set dist(20m) 4.80696e-07
set dist(25m) 3.07645e-07
set dist(30m) 2.13643e-07
set dist(35m) 1.56962e-07
set dist(40m) 1.20174e-07
set dist(100m) 7.214e-3
set dist(500m) 2.28289e-11

Phy/WirelessPhy set CSThresh_ $dist(500m)
Phy/WirelessPhy set RXThresh_ $dist(500m)


getopt $argc $argv

if { $opt(x) == 0 || $opt(y) == 0 } {
	usage $argv0
	exit 1
}

if {$opt(seed) > 0} {
	puts "Seeding Random number generator with $opt(seed)\n"
	ns-random $opt(seed)
}

#
# Initialize Global Variables
#
set ns_		[new Simulator]
set chan	[new $opt(chan)]
set prop	[new $opt(prop)]
set topo	[new Topography]


set tracefd   [open $opt(tr) w]
$ns_ trace-all  $tracefd

set namfile [open $opt(nam) w]
$ns_ namtrace-all-wireless $namfile $opt(x) $opt(y)

$topo load_flatgrid $opt(x) $opt(y)

$prop topography $topo

#
# Create God
set god_ [create-god $opt(nn)]
set chan_1_ [new $opt(chan)]
#
 
proc UniformErr {} {
set erm [new ErrorModel]
$erm unit packet
$erm set rate_ 0.5
$erm ranvar [new RandomVariable/Uniform]
$erm drop-target [new Agent/Null]
return $erm
}


#
#  Create the specified number of nodes $opt(nn) and "attach" them
#  the channel.
#  Each routing protocol script is expected to have defined a proc
#  create-mobile-node that builds a mobile node and inserts it into the
#  array global $node_($i)

#configure node
$ns_ node-config -adhocRouting gpsr \
			 -llType $opt(ll) \
			 -macType $opt(mac) \
			 -ifqType $opt(ifq) \
			 -ifqLen $opt(ifqlen) \
			 -antType $opt(ant) \
			 -propType $opt(prop) \
			 -phyType $opt(netif) \
			-channelType $opt(chan) \
			 -topoInstance $topo \
			 -agentTrace ON \
			 -routerTrace ON \
			 -macTrace ON \
			 -movementTrace OFF \
			 -IncomingErrProc UniformErr \
			 -energyModel EnergyModel \
                		-initialEnergy 1 \
                		-rxPower 0.1 \
                		-txPower 0.1\


		
source ./gpsr.tcl
for {set i 0} {$i < $opt(nn) } {incr i} {
    gpsr-create-mobile-node $i
    
}

#=======================================================================
# Provide initial (X,Y, for now Z=0) co-ordinates for mobilenodes
#=======================================================================
$node_(0) set X_ 150
$node_(0) set Y_ 300
$node_(0) set Z_ 0
$node_(1) set X_ 75
$node_(1) set Y_ 250
$node_(1) set Z_ 0
$node_(2) set X_ 150
$node_(2) set Y_ 250
$node_(2) set Z_ 0
$node_(3) set X_ 225
$node_(3) set Y_ 250
$node_(3) set Z_ 0
$node_(4) set X_ 50
$node_(4) set Y_ 225
$node_(4) set Z_ 0
$node_(5) set X_ 250
$node_(5) set Y_ 225
$node_(5) set Z_ 0
$node_(6) set X_ 75
$node_(6) set Y_ 150
$node_(6) set Z_ 0
$node_(7) set X_ 150
$node_(7) set Y_ 150
$node_(7) set Z_ 0
$node_(8) set X_ 225
$node_(8) set Y_ 150
$node_(8) set Z_ 0
$node_(9) set X_ 50
$node_(9) set Y_ 75
$node_(9) set Z_ 0
$node_(10) set X_ 75
$node_(10) set Y_ 50
$node_(10) set Z_ 0
$node_(11) set X_ 150
$node_(11) set Y_ 50
$node_(11) set Z_ 0
$node_(12) set X_ 225
$node_(12) set Y_ 50
$node_(12) set Z_ 0
$node_(13) set X_ 250
$node_(13) set Y_ 75
$node_(13) set Z_ 0
$node_(14) set X_ 25
$node_(14) set Y_ 275
$node_(14) set Z_ 0

#=======================================================================
# set mobility file
#=======================================================================


$ns_ at 0.0  "$node_(0) sscs startPANCoord "  ;# startPANCoord <txBeacon=1> <BO=3> <SO=3>
$ns_ at 0.4	"$node_(1) sscs startDevice 1 1 1"
$ns_ at 0.5	"$node_(2) sscs startDevice 1 1 1"
$ns_ at 0.6	"$node_(3) sscs startDevice 1 1 1"
$ns_ at 0.7	"$node_(4) sscs startDevice 1 1 1"
$ns_ at 0.8	"$node_(5) sscs startDevice 1 1 1"
$ns_ at 0.9	"$node_(6) sscs startDevice 1 1 1"
$ns_ at 1.0	"$node_(7) sscs startDevice 1 1 1"
$ns_ at 1.1	"$node_(8) sscs startDevice 1 1 1"
$ns_ at 1.2	"$node_(9) sscs startDevice 1 1 1"
$ns_ at 1.3	"$node_(10) sscs startDevice 1 1 1"
$ns_ at 1.4	"$node_(11) sscs startDevice 1 1 1"
$ns_ at 1.5	"$node_(12) sscs startDevice 1 1 1"
$ns_ at 1.6	"$node_(13) sscs startDevice 1 1 1"
$ns_ at 1.7	"$node_(14) sscs startDevice 0"

Mac/802_15_4 wpanNam PlaybackRate 3ms
$ns_ at $appTime1 "puts \"\nTransmitting data ...\n\""



# Setup traffic flow between nodes
proc cbrtraffic { src dst interval starttime } {
   global ns_ node_
   set udp_($src) [new Agent/UDP]
   eval $ns_ attach-agent \$node_($src) \$udp_($src)
   set null_($dst) [new Agent/Null]
   eval $ns_ attach-agent \$node_($dst) \$null_($dst)
   set cbr_($src) [new Application/Traffic/CBR]
   eval \$cbr_($src) set packetSize_ 50
   eval \$cbr_($src) set rate_ 50k
   eval \$cbr_($src) set interval_ $interval
   eval \$cbr_($src) set random_ 0
   eval \$cbr_($src) attach-agent \$udp_($src)
   eval $ns_ connect \$udp_($src) \$null_($dst)
   $ns_ at $starttime "$cbr_($src) start"
}
# yang dihapus
if {"$opt(traffic)" == "cbr"} {
   puts "\nTraffic: cbr"
   #Mac/802_15_4 wpanCmd ack4data on
   puts [format "Acknowledgement for data: %s" [Mac/802_15_4 wpanCmd ack4data]]
   $ns_ at $appTime1 "Mac/802_15_4 wpanNam PlaybackRate 0.50ms"
   $ns_ at [expr $appTime1 + 0.5] "Mac/802_15_4 wpanNam PlaybackRate 1.5ms"
   cbrtraffic 14 0 0.6 50




   $ns_ at $appTime1 "$ns_ trace-annotate \"(at $appTime1) cbr traffic from node 14 to node 0\""

   Mac/802_15_4 wpanNam FlowClr -p GPSR -c tomato
   Mac/802_15_4 wpanNam FlowClr -p ARP -c black
   Mac/802_15_4 wpanNam FlowClr -p MAC -c navy
   Mac/802_15_4 wpanNam FlowClr -p cbr -s 14 -d 0 -c black

}

# defines the node size in nam
for {set i 0} {$i < $opt(nn)} {incr i} {
     $ns_ initial_node_pos $node_($i) 20
}

# Tell nodes when the simulation ends
for {set i 0} {$i < $opt(nn) } {incr i} {
    $ns_ at $stopTime "$node_($i) reset";
}
$ns_ at $stopTime "stop"
$ns_ at $stopTime "puts \"\nNS EXITING...\n\""
$ns_ at $stopTime "$ns_ halt"
proc stop {} {
    global ns_ tracefd appTime opt env
    $ns_ flush-trace
    close $tracefd
    set hasDISPLAY 0
    foreach index [array names env] {
        #puts "$index: $env($index)"
        if { ("$index" == "DISPLAY") && ("$env($index)" != "") } {
                set hasDISPLAY 1
        }
    }
    if { ("$opt(nam)" == "gpsr50.nam") && ("$hasDISPLAY" == "1") } {
    	#exec nam gpsr50.nam &
	#exec awk -f energy.awk gpsr50.tr &
		    }
}
puts "\nStarting Simulation..."
$ns_ run